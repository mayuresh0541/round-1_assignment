<h1>Home > Digital Electronic Circuits > Analysis and Synthesis of Sequential Circuits using Basic Flip-Flops</h1>
<hr>

<h2>Theory</h2><br>

>  **1. Sequential Circuits :**  

- The logic circuits whose outputs at any instant of time depend not only on the present input but also on the past outputs are called <u>sequential circuits.</u><br>

- The simplest kind of sequential circuit which is capable of storing one bit of information is called latch.The operation of basic latch can be modified, by providing an additional control input that determines, when the state of the circuit is to be changed.<br>

- The latch with *additional control input* is called the **Flip-Flop**.The additional control input is *either the clock or enable input.*<br>

> **2. Different types of Flip-Flop:**  *There are four basic types, namely, **S-R, J-K, D and T** Flip-Flops.*<br>

<center><u><b>(1) S-R Flip-Flop</b></u></center>

<center>

![Clocked NOR-based S-R Flip-Flop
](photo1.jpg)

</center>

<center>Figure 1: Clocked NOR-based S-R Flip-Flop</center><br>

<center>

![NAND-based S-R Flip-Flop
](fig-2.jpg)

</center>

<center>Figure 2: NAND-based S-R Flip-Flop
</center><br>

<center>

![Typical wave-form in S-R Flip-Flop](fig-3.jpg)

</center>

<center>Figure 3: Typical wave-form in S-R Flip-Flop
</center><br>

<center>

| S | R | Q(t+1) |
|:----|----:|:----:|
|0|0|Q(t)|
|0|1|0|
|1|0|1|
1|1|Not Used|

</center>

<br>

<center>Figure 4: S-R Flip-Flop characteristic Table
</center><br>

**NOTE :** ***1.** Clk, S and R signals are input signals.*<br>
***2.** $\widetilde{Q}$ and Q are Output signals.*<br>
<br>

<center><b><u>(2) J-K Flip-Flop
</u></b></center>

<center>

![J-K Flip-Flop using S-R Flip-Flop](fig-4.jpg)
<br>

</center>

<center>Figure 1: J-K Flip-Flop using S-R Flip-Flop
</center>
<br>

<center>

![NAND based J-K Flip-Flop](fig-5.jpg)

</center>

<br>

<center>Figure 2: NAND based J-K Flip-Flop</center>

<br>

<center>

![Typical wave-form in J-K Flip-Flop](fig-6.jpg)

</center>

<br>

<center>Figure 3: Typical wave-form in J-K Flip-Flop
</center>

<br>

<center>

| J | K | Q(t+1) |
|:----|----:|:----:|
|0|0|Q(t)|
|0|1|0|
|1|0|1|
|1|1|$\widetilde{Q}$|

</center>

<br>

<center>Figure 4: J-K Flip-Flop characteristic Table</center>
<br>

<center><b><u>(3) D Flip-Flop</u></b></center>

<br>

<center>

![D Flip-Flop](fig-7.jpg)

</center>

<br>

<center>Figure 1: D Flip-Flop</center>

<br>

<center>

![NAND-based D Flip-Flop](fig-8.jpg)

</center>

<center>Figure 2: NAND-based D Flip-Flop
</center>

<br>

<center>

| D | Q(t+1) |
|:----:|:----:|
|0|0|
|1|1|

</center>

<br>

<center>Figure 3: D Flip-Flop characteristic Table</center>

<br>
<br>



> **3. Synthesis using Flip-Flop :**

<Justify>As a simple exersise,students can verify the operation of a serial (sequential) adder (1 bit full adder). Carry output of a one bit full adder can be fed back to the input of a D Flip-Flop.The output of this Flip=Flop can be fed back to the carry input of that adder.</justify>
<br>
<br>
<br>


<center>

![Verification of the functionality of a combinational circuit using seqential element(Flip-Flop)](fig-9.jpg)

</center>

<br>

<center>Figure 1: Verification of the functionality of a combinational circuit using seqential element(Flip-Flop)

</center>

<br>

<center>

![Gate diagram of combinational circuit(1 bit full adder)
](fig-10.jpg)

</center>

<br>

<center>Figure 2: Gate diagram of combinational circuit(1 bit full adder)
</center>

<br>

<center>

| A | B | C<sub>in</sub> | | S  | C<sub>out</sub> |
|----|----|----|----|----|----|
|0|0|0||0|0|
|0|0|1||1|0|
|0|1|0||1|0|
|0|1|1||0|1|
|1|0|0||1|0|
|1|0|1||0|1|
|1|1|0||0|1|
|1|1|1||1|1|

</center>

<br>

<center>Figure 3: Truth table of a 1 bit full adder</center>

<br>

<p style="text-align:right">
<h2>

 [Back](http://vlabs.iitkgp.ernet.in/dec/#)
</h2>
</p>

<hr>
<br>
<center>
<h2>

[Home](http://vlabs.iitb.ac.in/vlab/ "Click here to go to home")

<br>

<br>

![Vlabs](fig-11.jpg)

</h2>
</center>
<br>
<hr>