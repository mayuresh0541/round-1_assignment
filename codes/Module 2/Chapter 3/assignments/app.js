function check() {
    //consider a triangle whose vertices are A(x1,y1) , B(x2,y2) , C(x3,y3).
    //There is a point in the plane(which maybe inside the triangle or outside of it) whose co-ordinates are P(x,y).
    //So we have to find the position of point P(x,y) i.e; if that point lies inside or outside the triangle.
    let t11 = document.getElementById("t11"); //to define text area for point x1
    let t12 = document.getElementById("t12"); //to define text area for point y1
    let t21 = document.getElementById("t21"); //to define text area for point x2
    let t22 = document.getElementById("t22"); //to define text area for point y2
    let t31 = document.getElementById("t31"); //to define text area for point x3
    let t32 = document.getElementById("t32"); //to define text area for point y3
    let tx = document.getElementById("tx"); //to define text area for point x
    let ty = document.getElementById("ty"); //to define text area for point y
    var x1 = parseFloat(t11.value); //to access the number which is entered in the field of x1
    var y1 = parseFloat(t12.value); //to access the number which is entered in the field of y1
    var x2 = parseFloat(t21.value); //to access the number which is entered in the field of x2
    var y2 = parseFloat(t22.value); //to access the number which is entered in the field of y2
    var x3 = parseFloat(t31.value); //to access the number which is entered in the field of x3
    var y3 = parseFloat(t32.value); //to access the number which is entered in the field of y3
    var x = parseFloat(tx.value); //to access the number which is entered in the field of x
    var y = parseFloat(ty.value); //to access the number which is entered in the field of y
    var area_of_triangle; //declaration of variables
    var area_of_PAB;
    var area_of_PBC;
    var area_of_PAC;
    area_of_triangle = Math.abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2); //use of necessary formulae
    area_of_PAB = Math.abs((x * (y1 - y2) + x1 * (y2 - y) + x2 * (y - y1)) / 2);
    area_of_PBC = Math.abs((x * (y2 - y3) + x2 * (y3 - y) + x3 * (y - y2)) / 2);
    area_of_PAC = Math.abs((x * (y3 - y1) + x3 * (y1 - y) + x1 * (y - y3)) / 2);
    let sum = area_of_PAB + area_of_PBC + area_of_PAC;
    /*If sum of areas the triangles which are formed by taking point P as one vertex is equal to area of given triangle,
    then we can say the point is inside the triangle.
    If the sum exceeds area of triangle, then we can say that point lies outside the triangle.
    i.e;area of triangle formed by addition of 3 small triangles is greater than area of given triangle.*/
    if (sum <= area_of_triangle) {
        alert("Given point x=" + x + " and y=" + y + " lies INSIDE the triangle!!!");
    }
    else {
        alert("Given point " + x + " and " + y + " lies OUTSIDE the triangle!!!");
    }
}
//# sourceMappingURL=app.js.map