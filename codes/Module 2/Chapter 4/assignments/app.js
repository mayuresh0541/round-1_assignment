function dynamic() {
    //Here we have to construct a table which will tell us the table of entered number till entered number.
    var input = document.getElementById("input"); //to define the text area
    var table = document.getElementById("table_1"); //to create a table of id provided in html code as "table_1"
    var count = 1; //count for table
    var num = +input.value; //value from user
    while (table.rows.length > 1) {
        table.deleteRow(1); //to delete rows except one i.e; header row
    }
    for (count = 1; count <= num; count++) //to traverse till no. entered by user comes
     {
        var row = table.insertRow(); //to add one more row in every iteration
        var cell = row.insertCell(); //to add columns to the row
        var text = document.createElement("input"); //to create text field
        text.type = "text";
        text.style.textAlign = "center";
        text.style.border = "solid";
        text.style.background = "#1ab2ff";
        text.style.fontSize = "23px";
        text.value = num.toString(); //it will show the value of 'num' which has to be entered by user
        cell.appendChild(text);
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.style.border = "solid";
        text.style.background = "#ffcc99";
        text.style.fontSize = "23px";
        text.value = "X"; //to show the "X" sign in the text field for the whole column
        cell.appendChild(text);
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.style.border = "solid";
        text.style.background = "#1ab2ff";
        text.style.fontSize = "23px";
        text.value = count.toString(); //it will display the counter value which is incrementing by 1 in each iteration
        cell.appendChild(text);
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.style.border = "solid";
        text.style.background = "#ffff99";
        text.style.fontSize = "23px";
        text.value = "="; //to show the "X" sign in the text field for the whole column
        cell.appendChild(text);
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.style.border = "solid";
        text.style.background = "springgreen";
        text.style.fontSize = "23px";
        text.value = (num * count).toString(); //to display the table of given number i.e; multiplication of number and counter
        cell.appendChild(text);
    }
}
//# sourceMappingURL=app.js.map